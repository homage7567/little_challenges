module.exports = {
  arrowParens: 'avoid',
  trailingComma: 'none',
  bracketSameLine: true,
  overrides: [
    {
      files: ['*.js'],
      options: {
        singleQuote: true,
        jsxSingleQuote: true
      }
    }
  ]
};
