import { reverseString } from '../reverse-string';

const testCases = [
  { input: ['h', 'e', 'l', 'l', 'o'], expect: ['o', 'l', 'l', 'e', 'h'] }
];

testCases.forEach(testCase => {
  test(`should return ${testCase.expect} for ${testCase.input}`, () => {
    expect(reverseString(testCase.input)).toEqual(testCase.expect);
  });
});
